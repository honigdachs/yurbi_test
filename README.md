Yurbbi-Example-Chat
==================

A shop_id based chat application based on Tornado and Redis

This is an example of a mutli-shop_id chat application based on the asynchronous
web framework [Tornado](http://www.tornadoweb.org/). It uses [Redis Pup/Sub](http://redis.io/topics/pubsub)
feature as a message queue to distribute chat messages to multiple instances in a mutli-process
setup. 
It uses [brukva](https://github.com/evilkost/brukva) as asynchronous
Redis client. Client-Server communication is based on [websockets](http://www.tornadoweb.org/en/stable/websocket.html).



## Requirements and Setup
Python 2.7

First of all you need Redis:
```Bash
sudo apt-get install redis-server
```
Next you need the following python packages:
```Bash
sudo pip install tornado
sudo pip install git+https://github.com/evilkost/brukva.git
```
cd yurbi_chat
```

## Run Application
You can run the application on a specific port like that:
```Bash
python app.py --port=8888
```
Open `localhost:8888` in your browser. 
Sign in go to a shop message room and write something. 
Open another browser sign in with other credentials go to the same shop message room and start chatting in real time. 
